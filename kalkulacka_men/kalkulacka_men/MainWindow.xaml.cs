﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace kalkulacka_men
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            StahniKurz();
            StahniKurzUS();
        }

        private bool edituji = false; // bool proměnná pro identifikaci zapisování čísla
        private double kurz = 0; // defaultní nastavení kurzu EURA
        private double kurzUS = 0; // defaultní nastavení kurzu Dolaru
        private void tbxCZK_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (edituji) return;
            edituji = true;
            try
            {
                double czk = Convert.ToDouble(tbxCZK.Text);
                double eur = czk / kurz;
                double us = czk / kurzUS;
                tbxEUR.Text = eur.ToString("N2");
                tbxUS.Text = us.ToString("N2");
            }
            catch
            {
                tbxEUR.Text = "";
                tbxUS.Text = "";
            }
            edituji = false;
        }

        private void tbxEUR_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (edituji) return;
            edituji = true;
            try
            {
                double eur = Convert.ToDouble(tbxEUR.Text);
                double czk = eur * kurz;
                double us = czk / kurzUS;
                tbxCZK.Text = czk.ToString("N2");
                tbxUS.Text = us.ToString("N2");
            }
            catch
            {
                tbxCZK.Text = "";
                tbxUS.Text = "";
            }
            edituji = false;
        }
        private void tbxUS_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (edituji) return;
            edituji = true;
            try
            {
                double us = Convert.ToDouble(tbxUS.Text);
                double czk = us * kurzUS;
                double eur = czk / kurz;
                tbxCZK.Text = czk.ToString("N2");
                tbxEUR.Text = eur.ToString("N2");
            }
            catch
            {
                tbxCZK.Text = "";
                tbxEUR.Text = "";
            }

            edituji = false;
        }
        /**
         * Metoda pro stahnuti aktualniho kurzu eura ze stranek cnb a ulozeni hodnoty do promenne
         *
         **/
        private void StahniKurz()
        {
            WebClient wc = new WebClient();
            string s = wc.DownloadString(
                String.Format(
                "http://www.cnb.cz/miranda2/m2/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/vybrane.txt?mena=EUR&od={0}&do={1}",
                DateTime.Today.AddDays(-5).ToString("dd.MM.yyyy"), DateTime.Today.ToString("dd.MM.yyyy")));
            s = s.Substring(s.LastIndexOf('|') + 1);
            kurz = Convert.ToDouble(s);
        }
        /**
         * Metoda pro stahnuti aktualniho kurzu dolaru ze stranek cnb a ulozeni hodnoty do promenne
         *
         **/
        private void StahniKurzUS()
        {
            WebClient wc = new WebClient();
            string s = wc.DownloadString(
                String.Format("http://www.cnb.cz/miranda2/m2/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/vybrane.txt?mena=USD&od={0}&do={1}",
                DateTime.Today.AddDays(-5).ToString("dd.MM.yyyy"), DateTime.Today.ToString("dd.MM.yyyy")));
            s = s.Substring(s.LastIndexOf('|') + 1);
            kurzUS = Convert.ToDouble(s);
        }
    }
}